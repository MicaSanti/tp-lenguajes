class Gramatica():
    def __init__(self, gramatica):
        self.tabla = {}
        lista_producciones = []  # va a contener las reglas y los first de cada regla
        for indice, item in enumerate(gramatica.split("\n")):
            regla = item.split(":")
            antecedente = regla[0]
            consecuente = regla[1].split(" ")
            lista_producciones.append(
                (antecedente, consecuente, [], []))  # contendrá los firsts y selects por cada regla
            if indice == 0:
                self.distinguido = antecedente
        self.producciones = lista_producciones
        self.terminales = []
        self.no_terminales = []

    def isLL1(self):
        # diccionario que tendrá como clave cada NT, valores: firsts (del NT en general) y follows
        conjuntos = {}
        esLL1 = True  # devolverá True si es LL1 o False si no lo es. LO QUE RETORNA ESTA FUNCIÓN

        # FUNCIONES que se usan al calcular firsts y follows
        # ve si el noterminal deriva en lambda
        def tiene_lambda(noterminal):
            tiene_lambda = False
            for a, c, f, s in self.producciones:
                if a == noterminal and 'lambda' in c:
                    tiene_lambda = True
            return tiene_lambda

        # Busca los first cuando el consecuente empieza con un No Terminal
        def buscar_t(nt, dicc, lista):
            faltan = True
            while faltan == True:
                for fi in dicc[nt][0]:  # por cada item del conjunto
                    if fi in self.terminales:
                        lista.append(fi)
                    else:
                        buscar_t(fi, dicc, lista)
                faltan = False
            return lista

        # Detecta RI: recorre producciones y ve si el primer consecuente de una regla es igual a su antecedente
        for ant, cons, first, sel in self.producciones:
            if ant == cons[0]:
                esLL1 = False
                break

        antecedentes = []
        if esLL1:  # quiere decir que no tiene RI, entonces se calculan FIRSTS, FOLLOWS y SELECTS
            # Se crean las listas: "terminales" y "no_terminales"
            for ante, conse, first, sel in self.producciones:
                if ante not in antecedentes:
                    antecedentes.append(ante)
                if ante not in self.no_terminales:
                    self.no_terminales.append(ante)
                for c in conse:
                    if c[0].isupper():
                        if c not in self.no_terminales:
                            self.no_terminales.append(c)
                    else:
                        if c not in self.terminales:
                            self.terminales.append(c)

            # Detecta si un NT está en consecuente pero NO en algún antecedente
            for ante, conse, first, sel in self.producciones:
                for item in conse:
                    if item in self.no_terminales and item not in antecedentes:
                        esLL1 = False
                        break

        if esLL1:
            # se ponen como firsts los terminales que aparecen primero
            for ante, conse, first, sel in self.producciones:
                if ante not in conjuntos.keys():
                    # dos listas para firsts y follows
                    conjuntos[ante] = ([], [])
                if conse[0] in self.terminales:
                    first.append(conse[0])
                    conjuntos[ante][0].append(conse[0])  # agrego directamente
                else:  # primer elemento del consecuente es NT
                    for item in conse:  # recorro todo el consecuente para ver qué sigue
                        for ante2, conse2, first2, sel2 in self.producciones:  # por cada regla
                            if len(first) == 0 or tiene_lambda(first[-1]) == True:
                                first.append(item)
                                # agrego los first de los NT necesarios
                                conjuntos[ante][0].append(item)
                                break

            # Detectar RI indirecta
            for nt in conjuntos.keys():
                for nt2 in conjuntos.keys():
                    if nt in conjuntos[nt2][0] and nt2 in conjuntos[nt][0]:
                        esLL1 = False
                        break

        if esLL1:
            for ante, conse, first, sel in self.producciones:
                lista_quitar = []
                # si en firts hay NT, reemplazarlo por los firsts de ese
                for f in first:
                    if f in self.no_terminales:
                        # del diccionario obtenemos los firsts de ese NT
                        fi_nt = []
                        for fi in conjuntos[f][0]:
                            if fi in self.terminales:
                                fi_nt.append(fi)
                            else:  # si es NT busco los first de ese NT
                                l = []
                                for item in buscar_t(fi, conjuntos, l):
                                    fi_nt.append(item)  # función recursiva
                        first.extend(fi_nt)
                        lista_quitar.append(f)
                for item in lista_quitar:
                    first.remove(item)

            # Sacamos lambdas repetidos o que no corresponden
            for ante, conse, first, sel in self.producciones:
                # Si hay algún terminal en el consecuente, sacamos todos los lambda de los first
                # Si el consecuente = lambda, no se hace nada
                todosNT = True
                if len(conse) != 1:
                    for t in self.terminales:
                        if t in conse:
                            todosNT = False
                            borrar = []
                            for item in first:
                                if item == 'lambda':
                                    borrar.append(item)
                            for b in borrar:
                                first.remove(b)
                            break
                else:
                    if conse[0] in self.terminales:
                        todosNT = False
                # Si son todos No terminales, me fijo si el último deriva en lambda (si es así dejo uno solo y si no, borro todos)
                if todosNT:
                    if tiene_lambda(conse[-1]):
                        cant_lambda = first.count('lambda')
                        if cant_lambda > 1:
                            while first.count('lambda') > 1:
                                first.remove('lambda')
                    else:
                        borrar2 = []
                        for item in first:
                            if item == 'lambda':
                                borrar2.append(item)
                        for b2 in borrar2:
                            first.remove(b2)

            # elimina duplicados de los first
            for ante, conse, first, sel in self.producciones:
                lista_nueva = []
                for sublista in first:
                    if sublista not in lista_nueva:
                        lista_nueva.append(sublista)
                first.clear()
                for elemento in lista_nueva:
                    first.append(elemento)

            # FOLLOWS
            # Agregamos el $ en el distinguido
            conjuntos[self.distinguido][1].append('$')
            for nt in self.no_terminales:
                for ante1, conse1, first1, sel1 in self.producciones:
                    if nt in conse1:  # almacenamos en aux lo que hay después del NT en el consecuente
                        aux = [valor for indice, valor in enumerate(
                            conse1) if indice > conse1.index(nt)]
                        if len(aux) > 0:
                            for item in aux:
                                if item in self.terminales:
                                    conjuntos[nt][1].extend(item)
                                    break
                                else:  # NT
                                    firsts = []  # va a tener los firsts de los NT que siguen
                                    for ante, conse, first, sel in self.producciones:
                                        if ante == item:
                                            firsts.extend(first)
                                    if 'lambda' not in firsts:  # Si no tiene lambda el NT, break, no recorro más lo que sigue
                                        conjuntos[nt][1].extend(firsts)
                                        break
                                    else:
                                        firsts.remove('lambda')
                                        conjuntos[nt][1].extend(firsts)
                                    # Si el NT está al final del consecuente agrego el antecedente en los follow
                                    if item == aux[-1] and tiene_lambda(item):
                                        if ante1 not in conjuntos[nt][1]:
                                            conjuntos[nt][1].append(ante1)
                        # Si el NT está al final del consecuente agrego el antecedente en los follow
                        if len(aux) == 0:
                            if nt in conjuntos.keys():
                                if ante1 not in conjuntos[nt][1]:
                                    conjuntos[nt][1].append(ante1)

            # eliminamos los NT que hay en los follow en el caso que sean iguales que el antecedente (recursividad a derecha)
            for clave in conjuntos:
                if clave in conjuntos[clave][1]:
                    conjuntos[clave][1].remove(clave)

            # Casos en el que anteriormente agregamos el antecedente en los follow
            faltan_follows = True
            while faltan_follows:
                for clave in conjuntos:
                    for nt in self.no_terminales:
                        if nt in conjuntos[clave][1]:  # si hay un NT en los follows
                            # agrego los follow de ese NT pero si sus follow no tienen NT
                            if not any(self.no_terminales) in conjuntos[nt][1]:
                                conjuntos[clave][1].remove(nt)
                                conjuntos[clave][1].extend(conjuntos[nt][1])
                cant = len(conjuntos)
                # Controlo si faltan reemplazar NT en los follow
                for clave in conjuntos:
                    if not any(self.no_terminales) in conjuntos[clave][1]:
                        cant = cant - 1
                if cant == 0:
                    faltan_follows = False

            # sacamos duplicados de los follows
            for clave in conjuntos:
                nueva = []
                for item in conjuntos[clave][1]:
                    if item not in nueva:
                        nueva.append(item)
                conjuntos[clave][1].clear()
                for n in nueva:
                    conjuntos[clave][1].append(n)

            # SELECTS
            for ante, conse, first, sel in self.producciones:
                for f in first:
                    sel.append(f)
                if 'lambda' in sel:  # si hay lambda, lo sacamos y reemplazamos por los follows del NT
                    sel.remove('lambda')
                    follows = (conjuntos[ante][1])
                    for f in follows:
                        if f not in sel:
                            sel.append(f)

            # Verificamos si los select son disyuntos --> es LL1
            for clave in conjuntos:
                temporal = []  # para evaluar la intersección
                for ante, conse, first, sel in self.producciones:
                    if clave == ante:
                        for item in sel:  # por cada item del select
                            # si está en temporal
                            if item in temporal:  # quiere decir que la intersección NO ES VACÍA, no son disyuntos los selects para ese NT,
                                esLL1 = False  # no será LL1
                                break
                            else:
                                temporal.append(item)
                    if not esLL1:
                        break
                if not esLL1:
                    break

        # armado tabla para parse()
        if esLL1:
            for NT in self.no_terminales:
                self.tabla[NT, '$'] = ([])
                for term in self.terminales:
                    if term != 'lambda':
                        # por cada combinación de terminal y no terminal, colocamos los consecuentes
                        self.tabla[NT, term] = ([])
            for ante, conse, first, select in self.producciones:
                for se in select:
                    self.tabla[ante, se] = conse
        return esLL1

    def parse(self, cadena):
        # cadena a devolver, empieza siempre con distinguido
        entrada = cadena.split(" ")
        derivaciones = self.distinguido
        pila = []  # se irá modificando, lo que se inserta va al revés
        pila.append('$')
        pila.append(self.distinguido)
        lookahead = entrada[0]
        reconocida = True  # indica si cadena pertenece al lenguaje
        aceptada = False
        aux_deriv = []  # contiene lo que ya se reconoció de la cadena

        while reconocida:
            tope_pila = pila[-1]
            if tope_pila in self.no_terminales and (lookahead in self.terminales or lookahead == '$'):
                # consecuente por el que se reemplazará el NT
                consecuente = self.tabla[tope_pila, lookahead]
                if len(consecuente) != 0:  # si la celda NO está vacía
                    if 'lambda' in consecuente:  # sacar de tope de la pila y nada más
                        pila.pop()
                    else:
                        pila.pop()  # eliminar tope pila
                        # poner en la pila el consecuente por el que se reemplazó el NT
                        for item in reversed(consecuente):
                            pila.append(item)
                else:
                    reconocida = False
                if reconocida:
                    derivaciones = derivaciones + "=>"
                    for item in aux_deriv:
                        if item != "$":
                            derivaciones = derivaciones + item + " "
                    for item in reversed(pila):
                        if item != '$':
                            derivaciones = derivaciones + item + " "
                    if derivaciones[-1] == " ":
                        derivaciones = derivaciones[:-1]
            else:  # si en tope pila hay terminal o $
                if tope_pila == lookahead:
                    pila.pop()
                    aux_deriv.append(lookahead)
                    entrada.pop(0)  # consumir de la entrada
                    if tope_pila == '$' and lookahead == '$':
                        aceptada = True
                    else:
                        lookahead = entrada[0]
                else:
                    reconocida = False
            if aceptada:
                break  # cadena reconocida por la gramática
        if not reconocida:
            derivaciones = "La cadena NO pertenece al lenguaje"
        return derivaciones
